@extends('home')

@section('content')
<form action="/postingan/{{$postingan->id}}" enctype="multipart/form-data" method="POST">
    @csrf
    @method('put')

    <!--content-->

    <div class="form-group">
        <textarea name="content" value={{$postingan->content}} cols="30" rows="10" placeholder="What's on your mind?"></textarea>
        @error('content')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <br>

    <!--gambar-->
    <div class="form-group">
        <input type="file" class="form-control-file" name="gambar" value={{$postingan->gambar}}>
        @error('gambar')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
      </div>
      <br>

<!--quote-->
    <div class="form-group">
        <input type="text" class="form-control" name="quote" placeholder="Input quote" value={{$postingan->quote}}>
        @error('quote')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <br>

    <!--caption-->
    <div class="form-group">
        
        <input type="text" class="form-control" name="caption" placeholder="Input caption" value={{$postingan->caption}}>
        @error('caption')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
        <br>
        <br>

        <!--button-->
        <button type="submit" class="btn btn-primary">Post</button>
    </div>

    @endsection