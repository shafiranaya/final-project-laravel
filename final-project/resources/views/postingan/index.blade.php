@extends('template')

@section('content')

<form action="/postingan" method="POST">
@csrf
{{-- gatau kenapa, postan pertama gabisa dikomen --}}
<!--content-->
@foreach ($postingan as $value)

<div class="panel">
<h3>{{$value->content}}</h3>
<blockquote>{{$value->quote}}</blockquote>
<p><img src="{{asset('uploads/postingan/'.$value->gambar)}}" alt=""></p>
<caption>{{$value->caption}}</caption>
<br>
<!--button-->
<p>
<button><a href="/postingan/{{$value->id}}">Show</a></button>
<button><a href="/postingan/{{$value->id}}/edit">Edit</a></button>
<form action="/postingan/{{$value->id}}" method="POST">
    @csrf
    @method('DELETE')
    <input type="submit" class="btn btn-danger my-1" value="Delete">
</form>
</p>


{{-- komentar --}}
<form action="/komentar" method="POST">
    @csrf
    <input type="text" name="komentar" placeholder="Add a comment...">
    @error('content')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror

    <input type="hidden" name="postingan_id" value="{{$value['id']}}" placeholder="Post a comment">
    <button type="submit">Send</button>
</form>

<form action="/likepostingan" method="POST">
    @csrf
    <input type="hidden" name="postingan_id" value="{{$value['id']}}" placeholder="Post a comment">
    <button type="submit">Like Postingan</button>
</form>

<br>

</form>


@foreach ($value->komentar as $k)
<ul>
    @if ($k['postingan_id']==$value['id'])
    <li>
        <p>{{$k['komentar']}}
        -user{{$k['user_id']}}</p>

    <form action="/likekomentar" method="POST">
        @csrf
        <input type="hidden" name="komentar_id" value="{{$k['id']}}" placeholder="Like komentar">
        <button type="submit">Like Komentar</button>
    </form>
    </li>
    @endif
</ul>
@endforeach


@endforeach
</div>

@endsection