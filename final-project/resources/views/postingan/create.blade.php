@extends('home')

@section('content')
<form action="/postingan" enctype="multipart/form-data" method="POST">
    @csrf

    <!--content-->

    <div class="form-group">
        <textarea name="content" id="" cols="30" rows="10" placeholder="What's on your mind?"></textarea>
        @error('content')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <br>

    <!--gambar-->
    <div class="form-group">
        <input type="file" class="form-control-file" name="gambar">
        @error('gambar')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
      </div>
      <br>

<!--quote-->
    <div class="form-group">
        <input type="text" class="form-control" name="quote" placeholder="Input quote">
        @error('quote')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <br>

    <!--caption-->
    <div class="form-group">
        
        <input type="text" class="form-control" name="caption" placeholder="Input caption">
        @error('caption')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
        <br>
        <br>

        <!--button-->
        <button type="submit" class="btn btn-primary">Post</button>
    </div>

    @endsection