<form>
    <textarea placeholder="Whats in your mind today?" rows="2" class="form-control input-lg p-text-area"></textarea>
</form>
<footer class="panel-footer">
    <button class="btn btn-warning pull-right">Post</button>
    <ul class="nav nav-pills">
        <li>
            <a href="#"><i class="fa fa-map-marker"></i></a>
        </li>
        <li>
            <a href="#"><i class="fa fa-camera"></i></a>
        </li>
    </ul>
</footer>