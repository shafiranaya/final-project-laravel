@extends('home')

@section('title')
    Halaman Create Profile
@endsection

@section('content')

    <div class="ml-3 mt-3 mr-15">
        <form action="/profile" method="POST">
            @csrf
            <div class="form-group">
                <label>Nama Lengkap:</label>
                <input type="text" class="form-control" name="nama" placeholder="">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="form-group">
                <label>Biodata:</label>
                <input type="text" class="form-control" name="bio" placeholder="">
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <button type="submit" class="btn btn-primary">Create</button>
        </form>
    </div>

@endsection
