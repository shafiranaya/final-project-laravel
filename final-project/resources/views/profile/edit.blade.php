@extends('home')


@section('content')

    <div class="ml-3 mt-3 mr-15">
        <form action="/profile/{{$profile->id}}" method="POST">
        @method('put')
            @csrf
            <div class="form-group">
                <label>Nama Lengkap</label>
                <input type="text" class="form-control" name="nama" value="{{$profile->nama}}" placeholder="">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Biodata</label>
                <input type="text" class="form-control" name="bio" value="{{$profile->bio}}" placeholder="">
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
    </div>

@endsection
