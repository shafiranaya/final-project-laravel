<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/profile', 'ProfileController@index');

Route::get('/profile/create', 'ProfileController@create');

Route::post('/profile', 'ProfileController@store');

Route::get('/profile/{profile_id}', 'ProfileController@show');

Route::get('/profile/{profile_id}/edit', 'ProfileController@edit');

Route::put('/profile/{profile_id}', 'ProfileController@update');

Route::delete('/profile/{profile_id}', 'ProfileController@destroy');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('postingan','postinganController');

Route::post('/komentar', 'postinganController@createKomentar');
Route::post('/likepostingan', 'postinganController@createLikePostingan');
Route::post('/likekomentar', 'postinganController@createLikeKomentar');

// Route::get('/home/{id}', 'UserController@postinganUser');
// Route::post('/edit', 'UserController@edit');
// Route::get('/edit/{id}', 'UserController@edit');
// Route::post('/editProfile', 'UserController@edit');