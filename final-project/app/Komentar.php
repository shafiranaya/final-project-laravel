<?php
namespace App;
use Illuminate\Database\Eloquent\Model;


class Komentar extends Model {

  protected $table = "komentar";
  protected $fillable = ["user_id","postingan_id","komentar"];
  public $timestamps = false;

  public function postingan()
  {
   return $this->belongsToMany('App\postingan');
  }  
  public function user()
  {
   return $this->belongsToMany('App\User');
  }
  public function likeKomentar() {
    return $this->hasMany('App\LikeKomentar');
  }
}

?>