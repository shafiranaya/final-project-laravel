<?php
namespace App;
use Illuminate\Database\Eloquent\Model;


class LikePostingan extends Model {
  protected $table = "like_postingan";
  protected $fillable = ["user_id","postingan_id"];
  public $timestamps = false;

  public function postingan()
  {
   return $this->belongsToMany('App\Postingan');
  }  
  public function user()
  {
   return $this->belongsToMany('App\User');
  }
}
?>