<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class postingan extends Model
{
    public $timestamps=false;
    protected $table= 'postingan';
    protected $fillable= ['content', 'gambar', 'quote', 'caption'];

    public function komentar() {
        return $this->hasMany('App\Komentar');
    }

    public function likePostingan() {
        return $this->hasMany('App\LikePostingan');
    }
}
?>