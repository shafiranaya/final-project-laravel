<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\postingan;
use App\Komentar;
use App\User;
use App\LikePostingan;
use App\LikeKomentar;
use Auth;
use DB;
use File;

class postinganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $postingan= postingan::all();
        return view('postingan.index', compact('postingan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('postingan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'content' => 'required',
            'gambar' => 'mimes:jpeg,jpg,png|max:2200',
            'quote' => 'required',
            'caption' => 'required'
        ]);

       $gambar = $request->gambar;
       $new_gambar= time().'-'.$gambar->getClientOriginalName();

        $postingan=postingan::create([
            'content' =>$request->content,
            'gambar' => $new_gambar,
            'quote' => $request->quote,
            'caption' =>$request->caption
        ]);
        $gambar->move('uploads/postingan/', $new_gambar);
        return redirect ('/postingan');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $postingan=postingan::findorfail($id);
      return view('postingan.show', compact('postingan'));
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $postingan=postingan::findorfail($id);
        return view('postingan.edit', compact('postingan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'content' => 'required',
            'gambar' => 'mimes:jpeg,jpg,png|max:2200',
            'quote' => 'required',
            'caption' => 'required'
        ]);

        $postingan=postingan::findorfail($id);

        if($request->has('gambar')) {
            $path= "uploads/postingan/";
            File::delete($path.$postingan->gambar);
            $gambar = $request->gambar;

       $new_gambar= time().'-'.$gambar->getClientOriginalName();
       $gambar->move('uploads/postingan/', $new_gambar);
       $postingan_data=[
        'content' =>$request->content,
        'gambar' => $new_gambar,
        'quote' => $request->quote,
        'caption' =>$request->caption
       ];
      }else{
         $postingan_data =[
        'content' =>$request->content,
        'quote' => $request->quote,
        'caption' =>$request->caption
         ];
        }
        $postingan-> update($postingan_data);
        return redirect()->route('postingan.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $postingan=postingan::findorfail($id);
        $postingan->delete();
        $path="uploads/postingan/";
        file::delete($path.$postingan->gambar);
        return redirect()->route('postingan.index');
    }



    public function createKomentar(Request $request)
    {
        Komentar::create([
            'user_id' => Auth::user()->id,
            'postingan_id' => $request->postingan_id,
            'komentar' => $request->komentar
        ]);
        // $user = User::where('email', $request->email)->first();
        // $user = $user->getOriginal();
        // return redirect()->action(
        //     [UserController::class, 'Cookies'],
        //     ['id' => $user['email']]
        // );
        return redirect ('/postingan');
    }

    public function createLikePostingan(Request $request)
    {
        $islike = LikePostingan::where('postingan_id', $request->postingan_id)->where('user_id', Auth::user()->id)->first();
        if ($islike == NULL) {
            LikePostingan::create([
             'user_id' => Auth::user()->id,
            'postingan_id' => $request->postingan_id,
            // 'value' => true
            ]);
        } else {
            $islike->delete();
        }

        // $user = User::where('email', $request->email)->first();
        // $user = $user->getOriginal();
        // return redirect()->action(
        //     [UserController::class, 'Cookies'],
        //     ['id' => $user['email']]
        // );

        return redirect ('/postingan');
    }

    public function createLikeKomentar(Request $request)
    {
        $islike = LikeKomentar::where('komentar_id', $request->komentar_id)->where('user_id', Auth::user()->id)->first();
        if ($islike == NULL) {
            LikeKomentar::create([
             'user_id' => Auth::user()->id,
            'komentar_id' => $request->komentar_id,
            // 'value' => true
            ]);
        } else {
            $islike->delete();
        }

        // $user = User::where('email', $request->email)->first();
        // $user = $user->getOriginal();
        // return redirect()->action(
        //     [UserController::class, 'Cookies'],
        //     ['id' => $user['email']]
        // );

        return redirect ('/postingan');
    }

}
