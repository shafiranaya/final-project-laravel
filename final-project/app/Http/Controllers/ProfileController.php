<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ProfileController extends Controller
{

    public function index(){
        $profile = DB::table('profile')->get();
        return view('profile.index', compact('profile'));
    }

    public function create()
    {
        return view('profile.create');
    }

    public function store(Request $request)
    {
        // dd($request -> all());
        $request->validate([
            'nama_lengkap' => 'required|unique:post',
            'bio' => 'required',
        ]);
        $query = DB::table('profile')->insert([
            "nama_lengkap" => $request["nama_lengkap"],
            "bio" => $request["bio"]
        ]);
        return redirect('/profile');
    }

    public function show($id)
    {
        $profile = DB::table('profile')->where('id', $id)->first();
        return view('profile.show', compact('profile'));
    }

    public function edit($id)
    {
        $profile = DB::table('profile')->where('id', $id)->first();
        return view('profile.edit', compact('profile'));
    }

    public function update(Request $request, $id)
    {
        $profile = DB::table('profile')->where('id', $id)->first();
        $request->validate([
            "nama_lengkap" => $request["nama_lengkap"],
            "bio" => $request["bio"]
        ]);
        $query = DB::table('profile')
            ->where('id', $id)
            ->update([
                "nama_lengkap" => $request["nama_lengkap"],
                "bio" => $request["bio"]
            ]);
        return redirect('/profile');
    }
}
