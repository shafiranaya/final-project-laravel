<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use App\User;

class Follow extends Model {
  protected $table = "follow";
  protected $fillable = ["user_id1","user_id2"];
  public $timestamps = false;

  public function user1()
  {
   return $this->belongsToMany('App\User');
  }  
  public function user2()
  {
   return $this->belongsToMany('App\User');
  }
}
?>