<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class LikeKomentar extends Model {
  protected $table = "like_komentar";
  protected $fillable = ["user_id","komentar_id"];
  public $timestamps = false;

  public function komentar()
  {
   return $this->belongsToMany('App\Komentar');
  }  
  public function user()
  {
   return $this->belongsToMany('App\User');
  }
}
?>