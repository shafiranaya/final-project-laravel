<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTablePostingan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('postingan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('content');
            $table->string('gambar');
            $table->string('quote');
            $table->string('caption');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations....
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('postingan');
    }
}
