<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableLikeKomentar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('like_komentar', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->unsignedBigInteger('komentar_id');
            $table->foreign('komentar_id')->references('id')->on('komentar');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('like_komentar');
    }
}

// php artisan migrate:refresh --path=/database/migrations/2021_07_05_014749_create_table_like_postingan.php
// php artisan migrate:refresh --path=/database/migrations/2021_07_05_014951_create_table_komentar.php
// php artisan migrate:reset --path=/database/migrations/2021_07_05_014855_create_table_like_komentar.php
// php artisan migrate:refresh --path=/database/migrations/2021_07_05_014855_create_table_like_komentar.php
// php artisan migrate:refresh --path=/database/migrations/2021_07_05_015139_create_table_follow.php
