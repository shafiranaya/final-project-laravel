
# Final Project Kelompok 10

## Informasi kelompok
No. Kelompok: 10
<br>Anggota:
1. Shafira Naya (@shafiranaya)
2. Yola Yuliatri Mangera Putri (@yolaymputri)
3. Tarri Peritha Westi (@TarriPewe01)
<br>Tema: Sosial media

## ERD
![ERD](ERD.png)

## Link video demo
https://drive.google.com/file/d/1Ry0NzzSNpoyGcjbH96P1-h8H9q9ev2eh/view?usp=sharing
